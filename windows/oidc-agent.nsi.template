!define APPNAME "oidc-agent"
!define COMPANYNAME "oidc-agent"
!define DESCRIPTION "Commandline tools for obtaining OpenID Connect Access tokens"
INCLUDE VERSION

Var WebviewSetupError

# These will be displayed by the "Click here for support information" link in "Add/Remove Programs"
# It is possible to use "mailto:" links in here to open the email client
!define HELPURL "mailto:oidc-agent-contact@lists.kit.edu" # "Support Information" link
!define UPDATEURL "https://github.com/indigo-dc/oidc-agent/releases" # "Product Updates" link
!define ABOUTURL "https://github.com/indigo-dc/oidc-agent" # "Publisher" link
# This is the size (in kB) of all the files copied into "Program Files"
INCLUDE INSTALLSIZE

InstallDir "$PROGRAMFILES\${COMPANYNAME}\${APPNAME}"

# rtf or txt file - remember if it is txt, it must be in the DOS text format (\r\n)
LicenseData "license.txt"
# This will be in the installer/uninstaller's title bar
Name "${APPNAME}"
Icon "..\logos\logo.ico"
INCLUDE OUTFILE
 
!include LogicLib.nsh
 
# Just three pages - license agreement, install location, and installation
page license
page directory
Page instfiles
 
function .onInit
	setShellVarContext all
functionEnd
 
section "install"
    # Try to check if oidc-agent is running and inform user about restart
    #ExecShellWait "" "wmic" "process list brief | find /i 'oidc-agent.exe'" "SW_HIDE" $1
    ##wmic process list brief | find /i "oidc-agent.exe"
    #MessageBox MB_OK "oidc-agent: $1"
    #${If} $1 == "0"
    #    MessageBox MB_OK "oidc-agent will be restarted.    You will need to reenter your passphrase(s)."
    #${EndIf}
    #${If} $1 == "1"
    #    MessageBox MB_OK "oidc-agent will not be restarted."
    #${EndIf}


    # Kill old agent
    ExecShell "" "$INSTDIR\oidc-agent.exe" "-k" "SW_HIDE"
    Sleep 750
    ExecShell "" "wmic" "process where name='oidc-agent.exe' delete" "SW_HIDE"
    Sleep 750


	# Files for the install directory - to build the installer, these should be in the same directory as the install script (this file)
	setOutPath $INSTDIR
	# Files added here should be removed by the uninstaller (see section "uninstall")
    INCLUDE INSTALL_FILES

    setOutPath $LOCALAPPDATA\${COMPANYNAME}\${APPNAME}
    INCLUDE INSTALL_CONFIG_FILES

    setOutPath $LOCALAPPDATA\${COMPANYNAME}\${APPNAME}\issuer.config.d
    INCLUDE INSTALL_ISSUER_D_FILES

	# Uninstaller - See function un.onInit and section "uninstall" for configuration
	writeUninstaller "$INSTDIR\uninstall.exe"

    # Clean up the menu from older versions
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\*.lnk"
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account\*.lnk"
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access TOken\*.lnk"
 
	# Start Menu
    createDirectory "$SMPROGRAMS\${COMPANYNAME}"
    createDirectory "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}"
    #createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Start oidc-agent.lnk" "$INSTDIR\oidc-agent.exe" "" "$INSTDIR\logo.ico"

    createDirectory "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account"
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account\Add EGI Account.lnk" "cmd"             '/c "$INSTDIR\oidc-gen.exe"    --pub   --scope-max     --iss https://aai.egi.eu/auth/realms/egi                  && pause'   "$INSTDIR\logo.ico"
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account\Add WLCG Account.lnk" "cmd"            '/c "$INSTDIR\oidc-gen.exe"            --scope-max     --iss https://wlcg.cloud.cnaf.infn.it       --flow=device && pause'   "$INSTDIR\logo.ico"
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account\Add Helmholz-AAI Account.lnk" "cmd"    '/c "$INSTDIR\oidc-gen.exe"    --pub   --scope-max     --iss https://login.helmholtz.de/oauth2/                  && pause'   "$INSTDIR\logo.ico"
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account\Add Google Account.lnk" "cmd"          '/c "$INSTDIR\oidc-gen.exe"    --pub   --scope-max     --iss https://accounts.google.com/          --flow=device && pause'   "$INSTDIR\logo.ico"


    createDirectory "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access Token"
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access Token\Get Token for EGI.lnk" "cmd"            '/c "$INSTDIR\oidc-token.exe" https://aai.egi.eu/auth/realms/egi && pause'
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access Token\Get Token for WLCG.lnk" "cmd"           '/c "$INSTDIR\oidc-token.exe" https://wlcg.cloud.cnaf.infn.it    && pause'
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access Token\Get Token for Helmholtz-AAI.lnk" "cmd"  '/c "$INSTDIR\oidc-token.exe" https://login.helmholtz.de/oauth2/ && pause'
    createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access Token\Get Token for Google.lnk" "cmd"         '/c "$INSTDIR\oidc-token.exe" https://accounts.google.com/       && pause'

	# Registry information for add/remove programs
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayName" "${COMPANYNAME} - ${APPNAME} - ${DESCRIPTION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayIcon" "$\"$INSTDIR\logo.ico$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "Publisher" "$\"${COMPANYNAME}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "HelpLink" "$\"${HELPURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLUpdateInfo" "$\"${UPDATEURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLInfoAbout" "$\"${ABOUTURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayVersion" "$\"${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}$\""
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMajor" ${VERSIONMAJOR}
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMinor" ${VERSIONMINOR}
	# There is no option for modifying or repairing the install
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoRepair" 1
	# Set the INSTALLSIZE constant (!defined at the top of this script) so Add/Remove Programs can accurately report the size
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "EstimatedSize" ${INSTALLSIZE}

	# Add Install Location to PATH
    EnVar::AddValue "PATH" "$INSTDIR"
    Pop $0
    DetailPrint "EnVar::Check PATH returned={$0}"

    # Execute oidc-agent on startup
    WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Run" \
        "oidc-agent" "$INSTDIR\oidc-agent.exe"

    # Start oidc-agent
    #
    #!system "$INSTDIR\oidc-agent.exe"
    ExecShell "" "$INSTDIR\oidc-agent.exe" "" SW_HIDE
    #Exec "$INSTDIR\oidc-agent.exe"
    Pop $0
    DetailPrint "Started {$INSTDIR}oidc-agent.exe: {$0}"
    #!system "$INSTDIR\oidc-agent.exe" ERRLVL
    #DetailPrint "Started {$INSTDIR}oidc-agent.exe:  ${ERRLVL}"
    #

sectionEnd

Section "Webview2 Install"
 SectionIn RO

 DetailPrint "Checking for Webview2..."
 ReadRegStr $0  HKLM "SOFTWARE\WOW6432Node\Microsoft\EdgeUpdate\Clients\{F3017226-FE2A-4295-8BDF-00C3A9A7E4C5}" 'pv'
 ReadRegStr $1  HKCU "Software\Microsoft\EdgeUpdate\Clients\{F3017226-FE2A-4295-8BDF-00C3A9A7E4C5}" 'pv'

 ${If} $0 == ""
    ${AndIf} $1 == ""

 SetOutPath "$TEMP"
 File "webview2installer.exe"
 DetailPrint "Running Webview2 Setup..."
 ExecWait '"$TEMP\webview2installer.exe" /silent /install' $WebviewSetupError
 DetailPrint "Finished Webview2 Setup"

 Delete "$TEMP\webview2installer.exe"

 SetOutPath "$INSTDIR"

 ${EndIf}

SectionEnd

# Uninstaller
 
function un.onInit
	SetShellVarContext all
 
	#Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanently remove ${APPNAME}?" IDOK next
		Abort
	next:
functionEnd
 
section "uninstall"
    # Kill old agent
    ExecShell "" "$INSTDIR\oidc-agent.exe" "-k" "SW_HIDE"
    Sleep 750
    ExecShell "" "wmic" "process where name='oidc-agent.exe' delete" "SW_HIDE"
    Sleep 750
 
	# Remove Start Menu launcher
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\*.lnk"
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Create Account\*.lnk"
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\Obtain Access TOken\*.lnk"
    delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}\*"
	# Try to remove the Start Menu folder - this will only happen if it is empty
	rmDir "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}"
	rmDir "$SMPROGRAMS\${COMPANYNAME}"
 
	# Remove files
    delete "$INSTDIR\libffi-7.dll"
	INCLUDE UNINSTALL_FILES
	INCLUDE UNINSTALL_CONFIG_FILES
	INCLUDE UNINSTALL_ISSUER_D_FILES
  rmDir "$LOCALAPPDATA\${COMPANYNAME}\${APPNAME}\issuer.config.d"

	# Always delete uninstaller as the last action
	delete $INSTDIR\uninstall.exe
 
	# Try to remove the install directory - this will only happen if it is empty
	rmDir $INSTDIR

	# Remove Install Location from PATH
	EnVar::DeleteValue "PATH" "$INSTDIR"

	# Remove uninstaller information from the registry
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}"
sectionEnd
# vim: ft=nsis
