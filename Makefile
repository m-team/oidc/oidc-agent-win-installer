VERSION   ?= $(shell cat VERSION)
BINDIR   = bin
CONFDIR  = config
# Windows installer

.PHONY: win_create_installer_script
win_create_installer_script: windows/oidc-agent.nsi

windows/oidc-agent.nsi: windows/oidc-agent.nsi.template windows/make-nsi.sh windows/file-includer.sh windows/license.txt win_cp_dependencies
	@windows/make-nsi.sh

.PHONY: win_installer
win_installer: $(BINDIR)/installer.exe

.PHONY: win_binaries
win_binaries:

$(BINDIR)/installer.exe: windows/oidc-agent.nsi windows/license.txt windows/webview2installer.exe win_cp_dependencies
	@makensis -V4 -WX -NOCONFIG -INPUTCHARSET UTF8 -OUTPUTCHARSET UTF8 windows/oidc-agent.nsi

windows/license.txt: LICENSE
	@cp -p $< $@

windows/webview2installer.exe:
	@curl -L https://go.microsoft.com/fwlink/p/?LinkId=2124703 -s -o $@

-include windows/dependencies.mk
